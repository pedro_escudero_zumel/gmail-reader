package main

import (
        "fmt"
        "io/ioutil"
        "log"
        "strings"
        "encoding/base64"
        "golang.org/x/oauth2/google"
        "google.golang.org/api/gmail/v1"
)

type message struct {
	size    int64
	gmailID string
	date    string
	snippet string
}

func main() {
        b, err := ioutil.ReadFile("credentials.json")
        if err != nil {
                log.Fatalf("Unable to read client secret file: %v", err)
        }

        config, err := google.ConfigFromJSON(b, gmail.GmailReadonlyScope)
        if err != nil {
                log.Fatalf("Unable to parse client secret file to config: %v", err)
        }
        client := getClient(config)

        srv, err := gmail.New(client)
        if err != nil {
                log.Fatalf("Unable to retrieve Gmail client: %v", err)
        }

        var total int64
        	msgs := []message{}
          req := srv.Users.Messages.List("me")
          r, err := req.Do()

        for _, m := range r.Messages {
        			msg, err := srv.Users.Messages.Get("me", m.Id).Format("full").Do()
        			if err != nil {
        				log.Fatalf("Unable to retrieve message %v: %v", m.Id, err)
        			}
        			total += msg.SizeEstimate
        			date := ""
        			for _, h := range msg.Payload.Headers {

                if ( h.Name=="From" && strings.Contains(h.Value, "saracarrion0@gmail.com") ){ //WTF!
                  fmt.Printf("- from -%s \n", h.Value)
                  fmt.Printf("-  raw -%d \n", msg.Payload.Body.Size)
                  fmt.Printf("-  raw -%s \n\n", msg.Payload.Body.Data)
                  for _, part := range msg.Payload.Parts {
                      fmt.Println(part.MimeType)
                        data, _ := base64.StdEncoding.DecodeString(part.Body.Data)
                        html := string(data)
                        fmt.Println(html)
                    }
                  break;
                }

        			}

        			msgs = append(msgs, message{
        				size:    msg.SizeEstimate,
        				gmailID: msg.Id,
        				date:    date,
        				snippet: msg.Snippet,
        			})
        		}
}
